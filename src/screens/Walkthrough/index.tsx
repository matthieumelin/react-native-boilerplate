
import React, { useState } from 'react';
import { Link } from '@react-navigation/native';
import { Image, View, ScrollView, Text, Dimensions, StyleSheet, NativeSyntheticEvent, NativeScrollEvent } from 'react-native';
import 'nativewind';
const { width } = Dimensions.get('window');


const WalkthroughScreen = (): React.JSX.Element => {

    const [activeIndex, setActiveIndex] = useState(0);

    const handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
        const horizontalOffset = event.nativeEvent.contentOffset.x;
        const newIndex = Math.round(horizontalOffset / width);
        if (newIndex !== activeIndex) {
            setActiveIndex(newIndex);
        }
    };

    return (
        <View className='flex-1 bg-green-800'>


            <ScrollView
                horizontal
                pagingEnabled
                onScroll={handleScroll}
                showsHorizontalScrollIndicator={false}
                className='flex-1'

            >
                <View style={{ width }} className='flex justify-center items-stretch'>
                    <View className='items-center mb-5'>
                        <Image
                            style={{ height: 200, width: 200 }}
                            source={require('assets/analyse-de-sang.png')}
                        />
                    </View>
                    <Text className='text-center text-white text-2xl'>Page 1</Text>
                </View>
                <View style={{ width }} className='justify-center items-stretch'>
                    <View className='items-center mb-5'>
                        <Image
                            style={{ height: 200, width: 200 }}
                            source={require('assets/microscope.png')}
                        />
                    </View>
                    <Text className='text-center text-white text-2xl'>Page 2</Text>
                </View>
                <View style={{ width }} className='justify-center items-stretch'>
                    <View className='items-center mb-5'>
                        <Image
                            style={{ height: 200, width: 200 }}
                            source={require('assets/assistance-medicale.png')}
                        />
                    </View>
                    <Text className='text-center text-white text-2xl'>Page 3</Text>
                    <Link className='text-center text-white' to={{ screen: 'SignUp' }}>
                        Go subscribe
                    </Link>
                </View>
            </ScrollView>
            <View style={{ height: 50 }} >
                <View style={styles.pagination}>
                    {[...Array(3)].map((_, i) => (
                        <View
                            key={i}
                            style={[
                                styles.dot,
                                i === activeIndex
                                    ? styles.activeDot
                                    : styles.inactiveDot,
                            ]}
                        />
                    ))}
                </View>
            </View>


        </View>
    );
}

const styles = StyleSheet.create({
    pagination: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    dot: {
        height: 10,
        width: 10,
        borderRadius: 5,
        margin: 5,
    },
    activeDot: {
        backgroundColor: 'blue',
    },
    inactiveDot: {
        backgroundColor: 'gray',
    },
});

export default WalkthroughScreen;