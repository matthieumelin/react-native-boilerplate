
import { TouchableOpacity, StyleSheet } from 'react-native';
import auth from '@react-native-firebase/auth';
import { Formik, FormikHelpers, FormikProps } from 'formik';
import { Text } from 'react-native-paper';
import { theme } from 'theme';
import LoginForm, { LoginFields } from 'components/Forms/LoginForm';
import SignInValidation from 'components/Forms/LoginForm/validation';
import Screen from 'components/Screen';

interface ISignUpScreen {
    navigation: any
}


const SignInScreen: React.FC<ISignUpScreen> = ({ navigation }): React.JSX.Element => {


    const onSubmit = (values: LoginFields, helpers: FormikHelpers<LoginFields>) => {

        auth()
            .signInWithEmailAndPassword(values.email, values.password)
            .then(() => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                }

                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                }

                console.error(error);
            });

    }


    return (
        <Screen>
            <Text className='text-xl text-center'>Sign In</Text>
            <Formik initialValues={{ email: '', password: '' }} validationSchema={SignInValidation} onSubmit={onSubmit}>
                {(formikProps: FormikProps<LoginFields>) => <LoginForm {...formikProps} />}
            </Formik>

            <TouchableOpacity onPress={() => navigation.replace('PasswordLost')}>
                <Text className="text-center" style={styles.link}>Password lost</Text>
            </TouchableOpacity>


            <TouchableOpacity onPress={() => navigation.replace('SignUp')}>
                <Text className="text-center" style={styles.link}>Or Signup new account</Text>
            </TouchableOpacity>

        </Screen>
    );
}

const styles = StyleSheet.create({
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
})


export default SignInScreen;