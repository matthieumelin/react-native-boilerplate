
import Card from 'components/Card';
import Screen from 'components/Screen';
import Title from 'components/Title';
import { View, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Button } from 'react-native-paper';

interface IHomeScreen {

}

const HomeScreen: React.FC<IHomeScreen> = (): React.JSX.Element => {

    const navigation = useNavigation();

    return (
        <Screen>

            <Title>Bienveu Jérôme</Title>
            <Card clasN="mb-5">
                <Text className='text-center color-white p-5'>Commencer une évaluation</Text>
                <View className='m-5'>
                    {/* @ts-ignore */}
                    <Button mode="contained" onPress={() => { navigation.navigate("Measure") }}>S'évaluer</Button>
                </View>

            </Card>

            <Card>
                <Text className='text-center color-white p-5'>Progresser</Text>
                <View className='m-5'>
                    {/* @ts-ignore */}
                    <Button mode="contained" onPress={() => { navigation.navigate("Progress") }}>Progresser</Button>
                </View>

            </Card>

        </Screen>
    );
}

export default HomeScreen;