
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Formik, FormikHelpers, FormikProps } from 'formik';
import { Text } from 'react-native-paper';
import { theme } from 'theme';
import PasswordLostForm, { PasswordLostFields } from 'components/Forms/PasswordLostForm';
import PasswordLostValidation from 'components/Forms/PasswordLostForm/validation';

interface IPasswordLostScreen {
    navigation: any
}


const PasswordLostScreen: React.FC<IPasswordLostScreen> = ({ navigation }): React.JSX.Element => {


    const onSubmit = (values: PasswordLostFields, helpers: FormikHelpers<PasswordLostFields>) => {


    }

    return (
        <View className='flex-1 justify-center px-5'>
            <Text className='text-xl text-center'>Sign In</Text>
            <Formik initialValues={{ email: '' }} validationSchema={PasswordLostValidation} onSubmit={onSubmit}>
                {(formikProps: FormikProps<PasswordLostFields>) => <PasswordLostForm {...formikProps} />}
            </Formik>

            <TouchableOpacity onPress={() => navigation.replace('PasswordLost')}>
                <Text className="text-center" style={styles.link}>Password lost</Text>
            </TouchableOpacity>


            <TouchableOpacity onPress={() => navigation.replace('SignUp')}>
                <Text className="text-center" style={styles.link}>Or Signup new account</Text>
            </TouchableOpacity>

        </View>
    );
}

const styles = StyleSheet.create({
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
})


export default PasswordLostScreen;