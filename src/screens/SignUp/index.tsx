
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import auth from '@react-native-firebase/auth';
import { Formik, FormikHelpers, FormikProps } from 'formik';
import RegisterForm, { RegisterFields } from 'components/Forms/RegisterForm';
import SignUpValidation from 'components/Forms/RegisterForm/validation';
import { Text } from 'react-native-paper';
import { theme } from 'theme';
import Screen from 'components/Screen';

interface ISignUpScreen {
    navigation: any
}


const SignUpScreen: React.FC<ISignUpScreen> = ({ navigation }): React.JSX.Element => {


    const onSubmit = (values: RegisterFields, helpers: FormikHelpers<RegisterFields>) => {

        auth()
            .createUserWithEmailAndPassword(values.email, values.password)
            .then(() => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                }

                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                }

                console.error(error);
            });

    }


    return (

        <Screen>
            <Text className='text-xl text-center color-white'>Sign Up</Text>
            <Formik initialValues={{ email: '', password: '', confirmPassword: '' }} validationSchema={SignUpValidation} onSubmit={onSubmit}>
                {(formikProps: FormikProps<RegisterFields>) => <RegisterForm {...formikProps} />}
            </Formik>

            <TouchableOpacity onPress={() => navigation.replace('SignIn')}>
                <Text className="text-center" style={styles.link}>Already have an account</Text>
            </TouchableOpacity>

        </Screen>

    );
}

const styles = StyleSheet.create({
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
})


export default SignUpScreen;