
import Button from 'components/Buttons';
import Card from 'components/Card';
import Screen from 'components/Screen';
import SubTitle from 'components/SubTitle';
import Title from 'components/Title';
import { View, Text, ScrollView } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from 'theme';

const ProgressScreen = (): React.JSX.Element => {
    return (
        <Screen>
            <ScrollView>
                <Title>Progresser</Title>
                <Card>
                    <Text className='text-center color-white p-5'>Fixer un objectif santé</Text>


                </Card>
                <View className='m-5'>
                    <Button mode="contained">Définissez un objectif santé</Button>
                </View>
                <View className='flex flex-1 mt-10'>
                    <SubTitle>Explorer</SubTitle>
                    <Card>
                        <TouchableRipple>
                            <View className='flex flex-row p-5 items-center' style={{ borderBottomWidth: 1, borderBottomColor: theme.colors.outline }}>
                                <Icon name="chef-hat" size={20} color="#fff" />
                                <Text className='ml-2 color-white'>Recettes</Text>
                            </View>
                        </TouchableRipple>
                        <TouchableRipple>
                            <View className='flex flex-row p-5  items-center' style={{ borderBottomWidth: 1, borderBottomColor: theme.colors.outline }}>
                                <Icon name="abacus" size={20} color="#fff" />
                                <Text className='ml-2 color-white'>Entrainements</Text>
                            </View>
                        </TouchableRipple>
                        <TouchableRipple>
                            <View className='flex flex-row p-5  items-center'>
                                <Icon name="post" size={20} color="#fff" />
                                <Text className='ml-2 color-white'>Articles</Text>
                            </View>
                        </TouchableRipple>

                    </Card>
                </View>
            </ScrollView>
        </Screen>
    );
}

export default ProgressScreen;