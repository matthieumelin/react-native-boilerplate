
import { View, Text } from 'react-native';

const SettingScreen = (): React.JSX.Element => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Setting Screen</Text>
        </View>
    );
}

export default SettingScreen;