
import Card from 'components/Card';
import Screen from 'components/Screen';
import SubTitle from 'components/SubTitle';
import Title from 'components/Title';
import { ScrollView, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Button, Text, TouchableRipple } from 'react-native-paper';
import { theme } from 'theme';

interface IMeasureScreen {

}

const MeasureScreen: React.FC<IMeasureScreen> = (): React.JSX.Element => {
    return (
        <Screen>
            <ScrollView>
                <Title>Mesurer</Title>
                <Card>
                    <Text className='text-center color-white p-5'>Vous devez posséder un code entreprise pour passer une évaluation</Text>
                    <View className='m-5'>
                        <Button mode="contained">Ajouter un code</Button>
                    </View>

                </Card>
                <View className='flex flex-1 mt-10'>
                    <SubTitle>Vos précédents examens</SubTitle>
                    <Card>
                        <TouchableRipple>
                            <View className='flex flex-row p-5 items-center' style={{ borderBottomWidth: 1, borderBottomColor: theme.colors.outline }}>
                                <Icon name="download" size={20} color="#fff" />
                                <Text className='ml-2 color-white'>Evalaution 1</Text>
                            </View>
                        </TouchableRipple>
                        <TouchableRipple>
                            <View className='flex flex-row p-5  items-center'>
                                <Icon name="download" size={20} color="#fff" />
                                <Text className='ml-2 color-white'>Evalaution 2</Text>
                            </View>
                        </TouchableRipple>

                    </Card>
                </View>
            </ScrollView>
        </Screen>
    );
}

export default MeasureScreen;