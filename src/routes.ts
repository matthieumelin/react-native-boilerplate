const routes = {
  // Connected routes
  HOME: 'Home',
  SETTINGS: 'Setting',
  MEASURE: 'Measure',
  PROGRESS: 'Progress',
  // Not Connected routes
  SIGNIN: 'SignIn',
  SIGNUP: 'SignUp',
  WALKTHROUGH: 'Walkthrough',
}

export default routes
