import * as React from 'react';
import { Drawer } from 'react-native-paper';

interface IUserMenu {

}

const UserMenu: React.FC<IUserMenu> = (): React.JSX.Element => {
    const [active, setActive] = React.useState('');

    return (
        <Drawer.Section title="Some title">
            <Drawer.Item
                label="First Item"
                active={active === 'first'}
                onPress={() => setActive('first')}
            />
            <Drawer.Item
                label="Second Item"
                active={active === 'second'}
                onPress={() => setActive('second')}
            />
        </Drawer.Section>
    );
};

export default UserMenu;