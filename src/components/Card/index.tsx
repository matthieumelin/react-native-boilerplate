

import React from 'react';
import { Card as CardPaper, CardProps } from 'react-native-paper';
import { theme } from 'theme';

interface ICard extends CardProps {
    children: React.ReactNode
    clasN?: string
}

const Card: React.FC<ICard> = ({ children, clasN }): React.JSX.Element => {

    return (
        <CardPaper mode="contained" style={{ backgroundColor: theme.colors.surfaceVariant }} className={clasN}>
            {children}
        </CardPaper>
    );
}

export default Card;