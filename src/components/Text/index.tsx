import { Text as TextPaper } from 'react-native-paper';
import { theme } from 'theme';

interface IText {
    children: React.ReactNode
    style?: any
    className?: string
}

const Text: React.FC<IText> = ({ children, style, className }): React.JSX.Element => {
    return (
        <TextPaper style={{ color: theme.colors.text }} className={className}>{children}</TextPaper>
    );
}

export default Text;