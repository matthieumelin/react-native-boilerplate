import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as React from 'react';
import { View } from 'react-native';
import { Text, TouchableRipple } from 'react-native-paper';
import { theme } from 'theme';
import UserModal from 'components/Modals/UserModal';


interface IHeaderNavigationScreen {

}

const HeaderNavigation: React.FC<IHeaderNavigationScreen> = (): React.JSX.Element => {

    const [isModalVisible, setModalVisible] = React.useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };


    return (
        <View className='flex tes flex-row justify-stretch' style={{ height: 40, backgroundColor: theme.colors.surfaceVariant, borderBlockColor: theme.colors.outline, borderBottomWidth: 1 }}>

            <View className='flex flex-row items-center'>
                <TouchableRipple className='px-2' onPress={toggleModal}>
                    <Icon name="account" size={20} color={theme.colors.text} />
                </TouchableRipple>
                <Text className='ml-5' style={{ color: theme.colors.text }}>Page en cours</Text>
            </View>
            <UserModal isVisible={isModalVisible} toggleModal={toggleModal} />

        </View>
    );
};

export default HeaderNavigation;