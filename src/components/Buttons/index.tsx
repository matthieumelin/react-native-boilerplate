import React from 'react'
import { StyleSheet } from 'react-native'
import { Button as PaperButton, ButtonProps } from 'react-native-paper'
import { theme } from 'theme'

interface IButtonProps extends ButtonProps {
    mode?: "text" | "outlined" | "contained" | undefined
    children: React.ReactNode
}

const Button: React.FC<IButtonProps> = ({ mode, style, children, ...props }): React.JSX.Element => {
    return (
        <PaperButton
            style={[
                styles.button,
                mode === 'outlined' && { backgroundColor: theme.colors.surface },
                style,
            ]}
            labelStyle={styles.text}
            mode={mode}
            {...props}
        >{children}</PaperButton>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '100%',
        marginVertical: 10,
        paddingVertical: 2,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 26,
    },
})

export default Button
