
import { Text } from 'react-native';
import { theme } from 'theme';

interface ITitle {
    children: React.ReactNode
}

const Title: React.FC<ITitle> = ({ children }): React.JSX.Element => {
    return (
        <Text className='text-2xl font-bold mb-5' style={{ color: theme.colors.text }}>{children}</Text>
    );
}

export default Title;