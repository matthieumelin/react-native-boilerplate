import auth from '@react-native-firebase/auth';
import React from "react";
import { View } from "react-native";
import Modal from "react-native-modal";
import { Button, Card, Text, TouchableRipple } from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from "theme";

interface IUserModal {
    isVisible: boolean
    toggleModal: () => void
}

const UserModal: React.FC<IUserModal> = ({ isVisible, toggleModal }): React.JSX.Element => {

    const logOut = () => {

        auth()
            .signOut()
            .then(() => console.log('User signed out!'));
    }

    return (

        <Modal isVisible={isVisible}>
            <Card style={{ backgroundColor: theme.colors.surface }} className="mt-20 flex-1">
                <View className='flex flex-row items-center rounded-t' style={{ height: 50, backgroundColor: theme.colors.surfaceVariant }}>
                    <TouchableRipple className='px-2' onPress={toggleModal}>
                        <Icon name="close" size={30} color={theme.colors.text} />
                    </TouchableRipple>
                    <Text className='ml-5' style={{ color: theme.colors.text }}>Profil</Text>
                </View>

                <View className='flex flex-row rounded-t' style={{ height: 40 }}>
                    <Button mode="contained" onPress={logOut}>Deconnexion</Button>
                </View>

            </Card>
        </Modal>

    )

}

export default UserModal;