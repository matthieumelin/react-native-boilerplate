
import TextInput from 'components/Fields/TextInput';
import { FormikProps } from 'formik'
import { View } from 'react-native';
import { Button } from 'react-native-paper';

export interface LoginFields {
    email: string
    password: string
}

interface LoginFormProps extends FormikProps<LoginFields> { }

const LoginForm: React.FC<LoginFormProps> = ({ submitForm }) => {

    return (
        <View>
            <TextInput name="email" keyboardType="email-address" placeholder="Email" />
            <TextInput name="password" secureTextEntry placeholder="Mot de passe" />
            <Button
                mode="contained"
                onPress={submitForm}
                style={{ marginTop: 24 }}
            >
                Sign Up
            </Button>
        </View>
    )

}

export default LoginForm;
