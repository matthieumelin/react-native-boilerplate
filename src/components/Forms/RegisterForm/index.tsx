
import TextInput from 'components/Fields/TextInput';
import { FormikProps } from 'formik'
import { View } from 'react-native';
import { Button } from 'react-native-paper';

export interface RegisterFields {
    email: string
    password: string
    confirmPassword: string
}

interface RegisterFormProps extends FormikProps<RegisterFields> { }

const RegisterForm: React.FC<RegisterFormProps> = ({ submitForm }) => {

    return (
        <View>
            <TextInput name="email" keyboardType="email-address" placeholder="Votre email" />
            <TextInput name="password" secureTextEntry placeholder="Mot de passe" />
            <TextInput name="confirmPassword" secureTextEntry placeholder="Confirmer le mot de passe" />
            <Button
                mode="contained"
                onPress={submitForm}
                style={{ marginTop: 24 }}
            >
                Sign Up
            </Button>
        </View>
    )

}

export default RegisterForm;
