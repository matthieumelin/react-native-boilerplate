
import TextInput from 'components/Fields/TextInput';
import { FormikProps } from 'formik'
import { View } from 'react-native';
import { Button } from 'react-native-paper';

export interface PasswordLostFields {
    email: string
}

interface PasswordLostFormProps extends FormikProps<PasswordLostFields> { }

const PasswordLostForm: React.FC<PasswordLostFormProps> = ({ submitForm }) => {

    return (
        <View>
            <TextInput name="email" keyboardType="email-address" placeholder="Email" />
            <Button
                mode="contained"
                onPress={submitForm}
                style={{ marginTop: 24 }}
            >
                Sign Up
            </Button>
        </View>
    )

}

export default PasswordLostForm;
