import React from 'react'
import { Text } from 'react-native-paper'

interface FormErrorProps {
  error: string | string[]
}

const FormError: React.FC<FormErrorProps> = ({ error }) => {
  return (
    <>
      {Array.isArray(error) ? (
        error.map(errorItem => <Text className="text-red-500 text-sm">{errorItem}</Text>)
      ) : (
        <Text className="text-red-500 text-sm">{error}</Text>
      )}
    </>
  )
}

export default FormError
