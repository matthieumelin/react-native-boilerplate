import { TextInputProps as TextInputPropsPaper, TextInput as TextInputPaper } from "react-native-paper";
import { CustomFieldProps } from "types/form.types";
import { useField } from 'formik';
import { View, StyleSheet, Text } from 'react-native'
import { getError } from 'utils/form'
import { theme } from 'theme'
import FormError from "../FormError";



interface TextInputProps extends TextInputPropsPaper {
    icon?: JSX.Element | string
    name: string
}

const TextInput: React.FC<TextInputProps> = ({ name, icon, ...props }) => {


    const [value, meta, helpers] = useField(name)

    const handleChange = (new_text: string) => {
        helpers.setValue(new_text)
    }

    const handleBlur = (field: string) => () => {
        helpers.setTouched(true)
    }

    return (
        <View style={styles.container}>
            <TextInputPaper
                onChangeText={handleChange}
                onBlur={handleBlur(name)}
                autoCapitalize='none'
                style={styles.input}
                selectionColor={theme.colors.primary}
                underlineColor="transparent"
                mode="outlined"
                value={value.value}
                textColor="white"
                {...props}
            />

            {meta.error && meta.touched && <FormError error={meta.error} />}

        </View>
    )

}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 12,
    },
    input: {
        backgroundColor: theme.colors.surface,
    },
    description: {
        fontSize: 13,
        color: theme.colors.secondary,
        paddingTop: 8,
    },
    error: {
        fontSize: 13,
        color: theme.colors.error,
        paddingTop: 8,
    },
})

export default TextInput;