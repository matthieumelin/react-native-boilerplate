
import { View, Text } from 'react-native';
import { theme } from 'theme';

interface IScreen {
    children: React.ReactNode

}

const Screen: React.FC<IScreen> = ({ children }): React.JSX.Element => {
    return (
        <View className='flex flex-1 p-5' style={{ backgroundColor: theme.colors.surface }}>
            {children}
        </View>
    );
}

export default Screen;