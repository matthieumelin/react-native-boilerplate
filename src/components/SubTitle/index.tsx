
import { Text } from 'react-native';
import { theme } from 'theme';

interface ISubTitle {
    children: React.ReactNode
}

const SubTitle: React.FC<ISubTitle> = ({ children }): React.JSX.Element => {
    return (
        <Text className='text-xl font-bold mb-5' style={{ color: theme.colors.text }}>{children}</Text>
    );
}

export default SubTitle;