import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as React from 'react';
import { View } from 'react-native';
import { Text, TouchableRipple } from 'react-native-paper';
import { theme } from 'theme';


interface IPasswordLostScreen {

}



const BottomNavigation: React.FC<IPasswordLostScreen> = (): React.JSX.Element => {

    const navigation = useNavigation();

    return (
        <View className='flex flex-row justify-stretch pt-2' style={{ height: 75, backgroundColor: theme.colors.background, borderTopColor: theme.colors.outline, borderTopWidth: 1 }}>

            {/* @ts-ignore */}
            <TouchableRipple className="flex-1 " onPress={() => { navigation.navigate("Home") }}>
                <View className='items-center'>
                    <Icon name="home" size={30} color="#fff" />
                    <Text className='text-center color-white'>Home</Text>
                </View>
            </TouchableRipple>
            {/* @ts-ignore */}
            <TouchableRipple className="flex-1 " onPress={() => { navigation.navigate("Measure") }}>
                <View className='items-center'>
                    <Icon name="google-analytics" size={30} color="#fff" />
                    <Text className='text-center color-white'>Mesurer</Text>
                </View>
            </TouchableRipple>
            {/* @ts-ignore */}
            <TouchableRipple className="flex-1 " onPress={() => { navigation.navigate("Progress") }}>
                <View className='items-center'>
                    <Icon name="star" size={30} color="#fff" />
                    <Text className='text-center color-white'>Progresser</Text>
                </View>
            </TouchableRipple>


        </View>
    );
};

export default BottomNavigation;