import { FieldInputProps, FormikProps, FormikValues } from 'formik'

const accessDataByFieldName = (name: string, object: any) => {
  const sections = name.split('.')

  return sections.reduce((data, property) => {
    if (!data || !data[property]) {
      return undefined
    }

    return data[property]
  }, object)
}

export const getError = ({ form, field }: { form?: FormikProps<FormikValues>; field?: FieldInputProps<any> }) => {
  if (!field || !form) {
    return undefined
  }

  const isTouched = accessDataByFieldName(field.name, form.touched)
  const error = accessDataByFieldName(field.name, form.errors)

  return isTouched ? (error as string | string[]) : undefined
}
