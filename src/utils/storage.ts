import AsyncStorage from '@react-native-async-storage/async-storage';

export const save = async (key: string, value: any) => {

  try {
    const newValue = JSON.stringify(value)
    await AsyncStorage.setItem(key, newValue);
  } catch (e) {
    // saving error
  }

}

export const get = async (key: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
  }
}

export const destroy = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key)
  } catch (e) {
    // remove error
  }
}
