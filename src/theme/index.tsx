import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#fff',
    primary: '#263368',
    secondary: '#e5bc2c',
    error: '#f13a59',
    outline: '#45413e',
    background: '#000000',
    surfaceVariant: '#2c2b27',
    surface: '#0f0f0f',
    elevation: {
      level1: '#2c2b27',
    }
  },
}
