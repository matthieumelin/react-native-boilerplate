export interface IParams {
  exemple?: string
}

export enum EActionTypes {
  SET_PARAMS = 'SET_PARAMS',
}

export type ParamsPayload = {
  [EActionTypes.SET_PARAMS]: IParams
}

export const setParams = (params: IParams) => {
  return {
    type: EActionTypes.SET_PARAMS,
    payload: params,
  }
}
