import { ActionMap } from '../index'
import { EActionTypes, ParamsPayload, IParams } from './actions'

export type ParamsActionTypes = ActionMap<ParamsPayload>[keyof ActionMap<ParamsPayload>]

export interface IParamsState {
  params: IParams | null
}

export const initialState: IParamsState = {
  params: null
}


const reducer = (state: IParamsState, action: ParamsActionTypes): IParamsState => {
  switch (action.type) {
    case EActionTypes.SET_PARAMS:
      return {
        ...state,
        params: action.payload
      }

    default:
      return {
        ...state
      }
  }
}

export default reducer
