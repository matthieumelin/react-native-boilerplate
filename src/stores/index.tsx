import React, { createContext, useContext, useReducer } from 'react'
import { applyPersistToInitialState } from './persist'
import paramReducer, { initialState as paramsInitialState, IParamsState } from './params/reducer'

interface IStore {
  params: IParamsState
}

export type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
  ? {
    type: Key
  }
  : {
    type: Key
    payload: M[Key]
  }
}

interface IContextProps {
  state: IStore
  dispatch: React.Dispatch<any>
}

const StoreContext = createContext({} as IContextProps)

interface IStoreProvider {
  children: React.ReactNode
}

const mainReducer = ({ params }: IStore, action: any) => ({
  params: paramReducer(params, action)
})

export const StoreProvider: React.FC<IStoreProvider> = ({ children }) => {
  const [state, dispatch] = useReducer(
    mainReducer,
    applyPersistToInitialState({
      params: paramsInitialState
    })
  )

  return <StoreContext.Provider value={{ state, dispatch }}>{children}</StoreContext.Provider>
}

export const useStore = () => useContext(StoreContext)
