import { useEffect } from 'react'
import { save, get } from '../utils/storage'


export const persistKey = 'store_persist'

export function applyPersistToInitialState<S extends {}>(initialState: S): S {
  const clone = Object.assign({}, initialState)

  let persistedStore: S = clone

  get(persistKey).then((persistedStore) => {

    if (persistedStore) {

      persistedStore = Object.keys(persistedStore).reduce((result, key) => {
        return { ...result, [key]: persistedStore[key] }
      }, clone)
    }

  }).catch((error) => {

  })

  return persistedStore


}

export function usePersist<S extends {}>(store: S, whitelist: string[], isConnected: boolean) {
  useEffect(
    () => {
      if (isConnected) {
        const persistObject = whitelist.reduce((result, key) => {
          return {
            ...result,
            [key]: store[key as keyof S]
          }
        }, {})

        save(persistKey, persistObject).then(() => {
        }).catch((error) => {
        })

      }
    },
    Object.keys(store)
      .filter(key => whitelist.includes(key))
      .map(key => {
        return store[key as keyof S]
      })
  )
}



