import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import auth from '@react-native-firebase/auth'
import { View, Text, SafeAreaView, StatusBar } from 'react-native';
import HomeScreen from 'screens/Home';
import SignInScreen from 'screens/SignIn';
import SignUpScreen from 'screens/SignUp';
import SettingScreen from 'screens/Settings';
import WalkthroughScreen from 'screens/Walkthrough';
import PasswordLostScreen from 'screens/PasswordLost';
import BottomNavigation from 'components/BottomNavigation';
import MeasureScreen from 'screens/Measure';
import ProgressScreen from 'screens/Progress';
import HeaderNavigation from 'components/HeaderNavigation';
import { theme } from 'theme';
import UserMenu from 'components/UserMenu';
import { createDrawerNavigator } from '@react-navigation/drawer';


const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

const App = (): React.JSX.Element => {

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();


  const onAuthStateChanged = (user: any) => {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return <View><Text>Loading...</Text></View>;

  return (
    <SafeAreaView className='flex-1' style={{ backgroundColor: theme.colors.surface }}>
      <StatusBar
        animated={true}
        backgroundColor={theme.colors.surfaceVariant}
        hidden={false}
      />


      <NavigationContainer>
        {user && <HeaderNavigation />}

        <Stack.Navigator initialRouteName={user ? "Home" : "Walkthrough"}
          screenOptions={{ headerShown: false }}>

          {user ? (
            // Authenticated screens
            <>
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="Measure" component={MeasureScreen} />
              <Stack.Screen name="Progress" component={ProgressScreen} />
              <Stack.Screen name="Setting" component={SettingScreen} />

            </>
          ) : (
            // Not authenticated screens
            <>
              <Stack.Screen name="Walkthrough" component={WalkthroughScreen} />
              <Stack.Screen name="SignIn" component={SignInScreen} />
              <Stack.Screen name="SignUp" component={SignUpScreen} />
              <Stack.Screen name="PasswordLost" component={PasswordLostScreen} />
            </>
          )}


        </Stack.Navigator>
        {user && <BottomNavigation />}
      </NavigationContainer>
    </SafeAreaView >
  );
}


export default App;
