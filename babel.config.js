module.exports = {
  presets: ['module:@react-native/babel-preset'],
  plugins: [
    "nativewind/babel",
    [
      "module-resolver",
      {
        alias: {
          assets: "./src/assets",
          components: "./src/components",
          hooks: "./src/hooks",
          routes: "./src/routes",
          services: "./src/services",
          screens: "./src/screens",
          stores: "./src/stores",
          theme: "./src/theme",
          types: "./src/types",
          utils: "./src/utils",
        },
      }
    ]

  ],
  env: {
    production: {
      plugins: [
        'react-native-paper/babel'
      ],
    },
  },
};
